//program to find the volume of the cone where the user inputs the height and radius of the cone


#include<stdio.h>

int main()
{
	float height,radius,volume;
	const float PI=3.14;
	
	printf("Enter the height of the cone\n");
	scanf("%f",&height);
	
	printf("Enter the radius of the cone\n");
	scanf("%f",&radius);
	
	volume=(PI*radius*radius*height)/3;
	
	printf("Volume of the cone = %.2f",volume);
	
	return 0;
}
